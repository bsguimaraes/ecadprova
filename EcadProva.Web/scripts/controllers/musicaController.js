﻿var app = angular.module('app', ["checklist-model"]);
app.controller('musicaController', ['$scope', '$http','$location', musicaController]);

function musicaController($scope, $http, $location) {
    var value;
    if (!!$location.path()) {
        value = $location.path().split('/ID=')[1];
    }
    
    if (!!value) {
         //RETORNA A MÚSICA POR ID PARA EDIÇÃO
        $http({
            method: 'GET',
            url: 'https://localhost:44320/api/musica/' + parseInt(value),
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            if (!!response) {
                //$scope.Musica = response.data;
                $scope.Musica = {
                    id: response.data.id,
                    codigo: response.data.codigo,
                    nome: response.data.nome,
                    generoId: response.data.genero.id,
                    idsAutores: [...new Set(response.data.musicaAutores.map(item => item.idAutor))]
                }
            }
        }, function (error) {
            $scope.ErrorLista = "Erro ao listar as músicas"
        });
    } else {
        //RETORNA A LISTA DE MÚSICAS
        $http({
            method: 'GET',
            url: 'https://localhost:44320/api/musica',
            dataType: 'json',
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            if (!!response) {
                $scope.Musicas = response.data;
            }
        }, function (error) {
            $scope.ErrorLista = "Erro ao listar as músicas"
        });
    }

    //RETORNA A LISTA DE GENERO PARA PREENCHIMENTO DA COMBOBOX
    $http({
        method: 'GET',
        url: 'https://localhost:44320/api/genero',
        dataType: 'json',
        headers: { "Content-Type": "application/json" }
    }).then(function (response) {
        if (!!response) {
            $scope.Generos = response.data;
        }
    }, function (error) {
        $scope.Error = "Erro ao listar os gêneros"
        }); 

    //RETORNA A LISTA DE AUTORES PARA PREENCHIMENTO DA LISTAGEM
    $http({
        method: 'GET',
        url: 'https://localhost:44320/api/autor',
        dataType: 'json',
        headers: { "Content-Type": "application/json" }
    }).then(function (response) {
        if (!!response) {
            $scope.Autores = response.data;
        }
    }, function (error) {
        $scope.Error = "Erro ao listar os autores"
    }); 

    //FUNÇÃO PARA EDIÇÃO DA MÚSICA
    $scope.EditarMusica = function (index) {
        $scope.Musica = $scope.Musicas[index];
        $scope.Musica.index = index;
        $scope.Musica.editable = true;

        window.location.href = "../novaMusica.html#!/ID=" + $scope.Musica.id
    }

    //GRAVA A ALTERAÇÃO DA MÚSICA
    $scope.SalvarMusica = function (Musica) {
        $http({
            method: 'PUT',
            url: 'https://localhost:44320/api/musica/' + Musica.id,
            dataType: 'json',
            data: Musica,
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            if (!!response && response.status == 200) {
                $scope.Sucesso = "Alteração realizada com sucesso"
                delete $scope.Musica;
            }
        }, function (error) {
            $scope.Error = "Erro ao alterar a música."
        });
    }

    //FUNÇÃO PARA INCLUSÃO DE MÚSICA
    $scope.Incluir = function (Musica) {
        $http({
            method: 'POST',
            url: 'https://localhost:44320/api/musica',
            dataType: 'json',
            data: Musica,
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            if (!!response && response.status == 201) {
                $scope.Sucesso = "Inclusão realizada com sucesso"
                delete $scope.Musica;
            }
        }, function (error) {
            $scope.Error = "Erro ao adicionar a música."
        });
    }
}