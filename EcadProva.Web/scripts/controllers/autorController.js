﻿var app = angular.module('app', []);
app.controller('autorController', ['$scope', '$http', autorController]);

function autorController($scope, $http) {
    //RETORNA A LISTA DE AUTORES PARA PREENCHIMENTO DA LISTAGEM
    $http({
        method: 'GET',
        url: 'https://localhost:44320/api/autor',
        dataType: 'json',
        headers: { "Content-Type": "application/json" }
    }).then(function (response) {
        if (!!response) {
            $scope.Autores = response.data;
            $scope.Error = null;
        }
    }, function (error) {
        $scope.Error = "Erro ao listar os autores"
    }); 

    //FUNÇÃO PARA INCLUSÃO DE AUTOR
    $scope.Incluir = function (Autor) {
        $http({
            method: 'POST',
            url: 'https://localhost:44320/api/autor',
            dataType: 'json',
            data: Autor,
            headers: { "Content-Type": "application/json" }
        }).then(function (response) {
            console.log(response.data);
            if (!!response && response.status == 201) {
                $scope.Sucesso = "Inclusão realizada com sucesso"
                delete $scope.Autor;
                $scope.Error = null;
            }
        }, function (error) {
            $scope.Error = "Erro ao adicionar o autor."
        });
    }

    //RETORNA A LISTA DE CATEGORIA PARA PREENCHIMENTO DA COMBOBOX
    $http({
        method: 'GET',
        url: 'https://localhost:44320/api/categoria',
        dataType: 'json',
        headers: { "Content-Type": "application/json" }
    }).then(function (response) {
        if (!!response) {
            $scope.Categorias = response.data;
        }
    }, function (error) {
        $scope.Error = "Erro ao listar as categorias"
    }); 
}