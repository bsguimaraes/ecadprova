﻿using EcadProva.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace EcadProva.Infra.Data.Context
{
    public class EcadProvaContext : DbContext
    {
        public EcadProvaContext(DbContextOptions<EcadProvaContext> options)
            : base(options)
        {
        }

        public DbSet<Autor> Autor { get; set; }
        public DbSet<Categoria> Categoria { get; set; }
        public DbSet<Genero> Genero { get; set; }
        public DbSet<Musica> Musica { get; set; }
       
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Configuração da tabela AUTOR
            modelBuilder.Entity<Autor>(a =>
            {
                a.Property(au => au.Id).UseSqlServerIdentityColumn();
                a.HasOne(c => c.Categoria).WithMany(c => c.Autores).HasConstraintName("FK_Autor_Categoria").HasForeignKey(c => c.CategoriaId);
            });

            //Configuração da tabela MÚSICA
            modelBuilder.Entity<Musica>(m =>
            {
                m.Property(mu => mu.Id).UseSqlServerIdentityColumn();
                m.HasOne(g => g.Genero).WithMany(g => g.Musicas).HasConstraintName("FK_Musica_Genero").HasForeignKey(g => g.GeneroId);
            });

            //Configuração da tabela GÊNERO
            modelBuilder.Entity<Genero>(g => {
                g.Property(ge => ge.Id).UseSqlServerIdentityColumn();

                //Carga inicial na tabela de gênero
                g.HasData(new Genero
                {
                    Id = 1,
                    Nome = "Axe"
                }, new Genero
                {
                    Id = 2,
                    Nome = "Blues"
                }, new Genero
                {
                    Id = 3,
                    Nome = "Country"
                }, new Genero
                {
                    Id = 4,
                    Nome = "Eletrônica"
                }, new Genero
                {
                    Id =5,
                    Nome = "Forró"
                }, new Genero
                {
                    Id = 6, 
                    Nome = "Funk"
                }, new Genero
                {
                    Id = 7,
                    Nome = "Gospel"
                });
            });

            //Configuração da tabela CATEGORIA
            modelBuilder.Entity<Categoria>(g => {
                g.Property(ge => ge.Id).UseSqlServerIdentityColumn();

                //Carga inicial na tabela de categoria
                g.HasData(new Categoria
                {
                    Id = 1,
                    Nome = "Autor"
                }, new Categoria
                {
                    Id = 2,
                    Nome = "Compositor"
                }, new Categoria
                {
                    Id = 3,
                    Nome = "Intérprete"
                }, new Categoria
                {
                    Id = 4,
                    Nome = "Músico"
                });
            });

            //Configuração da tabela MÚSICAAUTOR
            modelBuilder.Entity<MusicaAutor>()
                .HasKey(a => new { a.IdMusica, a.IdAutor });

            modelBuilder.Entity<MusicaAutor>()
                .HasOne(a => a.Musica)
                .WithMany(a => a.MusicaAutores)
                .HasForeignKey(a => a.IdMusica);

            modelBuilder.Entity<MusicaAutor>()
                .HasOne(a => a.Autor)
                .WithMany(a => a.Musicas)
                .HasForeignKey(a => a.IdAutor);
        }
    }
}
