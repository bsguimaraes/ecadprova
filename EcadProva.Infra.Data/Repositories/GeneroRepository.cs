﻿using System.Collections.Generic;
using EcadProva.Domain.Interfaces.Repositories;
using EcadProva.Domain.Interfaces.Repositories.Base;
using EcadProva.Domain.Models;
using EcadProva.Infra.Data.Context;
using EcadProva.Infra.Data.Repositories.Base;

namespace EcadProva.Infra.Data.Repositories
{
    public class GeneroRepository : BaseRepository<Genero>, IGeneroRepository
    {
        public GeneroRepository(EcadProvaContext context) : base(context)
        {
        }
    }
}
