﻿using System.Collections.Generic;
using System.Linq;
using EcadProva.Domain.Interfaces.Repositories;
using EcadProva.Domain.Models;
using EcadProva.Infra.Data.Context;
using EcadProva.Infra.Data.Repositories.Base;
using Microsoft.EntityFrameworkCore;

namespace EcadProva.Infra.Data.Repositories
{
    public class AutorRepository : BaseRepository<Autor>, IAutorRepository
    {
        private DbSet<Autor> obj;

        public AutorRepository(EcadProvaContext context) : base(context)
        {
            obj = Db.Set<Autor>();
        }

        public override ICollection<Autor> Listar()
        {
            return obj.Include(x => x.Categoria).ToList();
        }
    }
}
