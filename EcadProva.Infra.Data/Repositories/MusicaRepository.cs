﻿using System.Collections.Generic;
using System.Linq;
using EcadProva.Domain.Interfaces.Repositories;
using EcadProva.Domain.Models;
using EcadProva.Infra.Data.Context;
using EcadProva.Infra.Data.Repositories.Base;
using Microsoft.EntityFrameworkCore;

namespace EcadProva.Infra.Data.Repositories
{
    public class MusicaRepository : BaseRepository<Musica>, IMusicaRepository
    {
        private DbSet<Musica> obj;

        public MusicaRepository(EcadProvaContext context) : base(context)
        {
            obj = Db.Set<Musica>();
        }

        public override ICollection<Musica> Listar()
        {
            return obj.Include(w => w.MusicaAutores).ThenInclude(x => x.Autor).Include(y=>y.MusicaAutores).ThenInclude(z=>z.Musica).ThenInclude(k=>k.Genero).ToList();
        }

        public override Musica RecuperarPorId(int id)
        {
            return obj.Where(x => x.Id  == id).Include(x => x.Genero).Include(z => z.MusicaAutores).ThenInclude(x => x.Autor).FirstOrDefault();
        }
    }
}
