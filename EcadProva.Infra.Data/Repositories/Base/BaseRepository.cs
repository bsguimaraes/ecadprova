﻿using System.Collections.Generic;
using System.Linq;
using EcadProva.Domain.Interfaces.Repositories.Base;
using EcadProva.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace EcadProva.Infra.Data.Repositories.Base
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected EcadProvaContext Db;

        public BaseRepository(EcadProvaContext context)
        {
            Db = context;
        }

        public virtual void Alterar(T obj)
        {
            Db.Entry(obj).State = EntityState.Modified;
            Db.SaveChanges();
        }

        public void Excluir(int id)
        {
            var entity = RecuperarPorId(id);
            Db.Set<T>().Remove(entity);
            Db.SaveChanges();
        }

        public void Incluir(T obj)
        {
            Db.Set<T>().Add(obj);
            Db.SaveChanges();
        }

        public virtual ICollection<T> Listar()
        {
            return Db.Set<T>().ToList();
        }

        public virtual T RecuperarPorId(int id)
        {
            return Db.Set<T>().Find(id);
        }
    }
}
