﻿using EcadProva.Domain.Interfaces.Repositories;
using EcadProva.Domain.Models;
using EcadProva.Infra.Data.Context;
using EcadProva.Infra.Data.Repositories.Base;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace EcadProva.Infra.Data.Repositories
{
    public class MusicaAutorRepository : BaseRepository<MusicaAutor>, IMusicaAutorRepository
    {
        private DbSet<MusicaAutor> obj;

        public MusicaAutorRepository(EcadProvaContext context) : base(context)
        {
            obj = Db.Set<MusicaAutor>();
        }

        //public void ExcluirAutoresPorMusica(MusicaAutor musicaAutores)
        //{
        //    obj.RemoveRange(musicaAutores);
        //    Db.SaveChanges();
        //}
        
        //public MusicaAutor ListarAutoresPorMusica(int idMusica)
        //{
        //    return obj.Where(x => x.IdMusica == idMusica).FirstOrDefault();
        //}

        public override MusicaAutor RecuperarPorId(int id)
        {
            return obj.Where(x => x.IdAutor == id).FirstOrDefault();
        }

        public MusicaAutor RetornarPorMusicaAutor(int idAutor, int idMusica)
        {
            return obj.Where(x => x.IdAutor == idAutor && x.IdMusica == idMusica).FirstOrDefault();
        }
    }
}
