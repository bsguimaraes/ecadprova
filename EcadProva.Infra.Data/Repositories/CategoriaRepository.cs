﻿using EcadProva.Domain.Interfaces.Repositories;
using EcadProva.Domain.Models;
using EcadProva.Infra.Data.Context;
using EcadProva.Infra.Data.Repositories.Base;

namespace EcadProva.Infra.Data.Repositories
{
    public class CategoriaRepository : BaseRepository<Categoria>, ICategoriaRepository
    {
        public CategoriaRepository(EcadProvaContext context) : base(context)
        {
        }
    }
}
