﻿using System.Collections.Generic;
using EcadProva.Domain.Interfaces.Repositories;
using EcadProva.Domain.Interfaces.Repositories.Base;
using EcadProva.Domain.Interfaces.Services.Base;

namespace EcadProva.Service.Base
{
    public abstract class BaseService<T> : IBaseService<T> where T : class
    {
        private readonly IBaseRepository<T> _repository;

        public BaseService(IBaseRepository<T> repository)
        {
            _repository = repository;
        }

        public virtual void Alterar(T obj)
        {
            _repository.Alterar(obj);
        }

        public void Excluir(int id)
        {
            _repository.Excluir(id);
        }

        public void Incluir(T obj)
        {
            _repository.Incluir(obj);
        }

        public virtual  ICollection<T> Listar()
        {
            return _repository.Listar();
        }

        public T RecuperarPorId(int id)
        {
            return _repository.RecuperarPorId(id);
        }
    }
}
