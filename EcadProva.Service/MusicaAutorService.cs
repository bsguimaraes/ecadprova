﻿using System.Collections.Generic;
using EcadProva.Domain.Interfaces.Repositories;
using EcadProva.Domain.Interfaces.Services;
using EcadProva.Domain.Models;
using EcadProva.Service.Base;

namespace EcadProva.Service
{
    public class MusicaAutorService : BaseService<MusicaAutor>, IMusicaAutorService
    {
        private IMusicaAutorRepository _repository;
        public MusicaAutorService(IMusicaAutorRepository repository) : base(repository)
        {
            _repository = repository;
        }

        public MusicaAutor RetornarPorMusicaAutor(int idAutor, int idMusica)
        {
            return _repository.RetornarPorMusicaAutor(idAutor, idMusica);
        }
    }
}
