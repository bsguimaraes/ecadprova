﻿using System.Collections.Generic;
using EcadProva.Domain.Interfaces.Repositories;
using EcadProva.Domain.Interfaces.Services;
using EcadProva.Domain.Models;
using EcadProva.Service.Base;

namespace EcadProva.Service
{
    public class MusicaService : BaseService<Musica>, IMusicaService
    {
        private IAutorService _autorService;
        private IMusicaRepository _musicaRepository;
        private IMusicaAutorService _musicaAutorService;

        public MusicaService(IMusicaRepository repository, IAutorService autorService, IMusicaAutorService musicaAutorService) : base(repository)
        {
            _autorService = autorService;
            _musicaRepository= repository;
            _musicaAutorService = musicaAutorService;
        }

        public void IncluirMusicaAutor(Musica musica, List<int> listaIDsAutor)
        {
            MusicaAutor musicaAutor;

            if(listaIDsAutor != null)
            {
                foreach (var id in listaIDsAutor)
                {
                    musicaAutor = new MusicaAutor();
                    musicaAutor.Autor = _autorService.RecuperarPorId(id);
                    musica.MusicaAutores.Add(musicaAutor);
                }
            }

            _musicaRepository.Incluir(musica);
        }

        public void AlterarMusicaAutor(Musica musica, List<int> listaIDsAutor)
        {
            MusicaAutor musicaAutor;

            if (listaIDsAutor != null)
            {
                foreach (var id in listaIDsAutor)
                {
                    var response = _musicaAutorService.RetornarPorMusicaAutor(id, musica.Id);

                    if (response != null) continue;
                    
                    musicaAutor = new MusicaAutor();
                    musicaAutor.Autor = _autorService.RecuperarPorId(id);
                    musicaAutor.IdAutor = id;
                    musicaAutor.IdMusica = musica.Id;
                    musica.MusicaAutores.Add(musicaAutor);
                }
            }

            _musicaRepository.Alterar(musica);
        }
    }
}
