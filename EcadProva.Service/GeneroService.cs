﻿using EcadProva.Domain.Interfaces.Repositories;
using EcadProva.Domain.Interfaces.Repositories.Base;
using EcadProva.Domain.Interfaces.Services;
using EcadProva.Domain.Models;
using EcadProva.Service.Base;

namespace EcadProva.Service
{
    public class GeneroService : BaseService<Genero>, IGeneroService
    {
        public GeneroService(IGeneroRepository repository) : base(repository)
        {
        }
    }
}
