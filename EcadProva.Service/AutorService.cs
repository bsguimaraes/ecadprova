﻿using EcadProva.Domain.Interfaces.Repositories;
using EcadProva.Domain.Interfaces.Services;
using EcadProva.Domain.Models;
using EcadProva.Service.Base;

namespace EcadProva.Service
{
    public class AutorService : BaseService<Autor>, IAutorService
    {
        public AutorService(IAutorRepository repository) : base(repository)
        {
        }
    }
}
