﻿using EcadProva.Domain.Interfaces.Repositories;
using EcadProva.Domain.Interfaces.Services;
using EcadProva.Domain.Models;
using EcadProva.Service.Base;

namespace EcadProva.Service
{
    public class CategoriaService : BaseService<Categoria>, ICategoriaService
    {
        public CategoriaService(ICategoriaRepository repository) : base(repository)
        {
        }
    }
}
