﻿using EcadProva.Domain.Interfaces.Repositories;
using EcadProva.Domain.Interfaces.Services;
using EcadProva.Infra.Data.Context;
using EcadProva.Infra.Data.Repositories;
using EcadProva.Service;
using Microsoft.Extensions.DependencyInjection;

namespace EcadProva.Infra.IoC
{
    public static class DIContainer
    {
        public static void RegisterDependencies(IServiceCollection services)
        {
            services.AddScoped<EcadProvaContext>();

            services.AddTransient<IMusicaService, MusicaService>();
            services.AddTransient<IMusicaAutorService, MusicaAutorService>();
            services.AddTransient<ICategoriaService, CategoriaService>();
            services.AddTransient<IAutorService, AutorService>();
            services.AddTransient<IGeneroService, GeneroService>();

            services.AddTransient<IMusicaRepository, MusicaRepository>();
            services.AddTransient<IMusicaAutorRepository, MusicaAutorRepository>();
            services.AddTransient<ICategoriaRepository, CategoriaRepository>();
            services.AddTransient<IAutorRepository, AutorRepository>();
            services.AddTransient<IGeneroRepository, GeneroRepository>();
        }

    }
}
