﻿using System.Collections.Generic;

namespace EcadProva.Api.ViewModel
{
    public class MusicaRequestViewModel
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Nome { get; set; }
        public int GeneroId { get; set; }
        public List<int> IdsAutores { get; set; }
    }
}
