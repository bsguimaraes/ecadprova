﻿namespace EcadProva.Api.ViewModel
{
    public class MusicaAutorViewModel
    {
        public int IdMusica { get; set; }
        public int IdAutor { get; set; }

        public virtual AutorViewModel Autor { get; set; }
        public virtual MusicaRequestViewModel Musica { get; set; }
    }
}
