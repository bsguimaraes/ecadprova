﻿using EcadProva.Domain.Models;

namespace EcadProva.Api.ViewModel
{
    public class AutorViewModel
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Nome { get; set; }
        public int CategoriaId { get; set; }
        public CategoriaViewModel Categoria { get; set; }
    }
}
