﻿using EcadProva.Domain.Models;
using System.Collections.Generic;
using System.Linq;

namespace EcadProva.Api.ViewModel
{
    public class MusicaResponseViewModel
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Nome { get; set; }
        public GeneroViewModel Genero { get; set; }
        public List<MusicaAutorViewModel> MusicaAutores { get; set; }

        public  string Autores {
            get
            {
                if (MusicaAutores != null)
                {
                    return string.Join(", ", MusicaAutores.Select(x => x.Autor.Nome));
                }

                return null;
            }
        }

    }
}
