﻿using AutoMapper;
using EcadProva.Api.ViewModel;
using EcadProva.Domain.Interfaces.Services;
using EcadProva.Domain.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace EcadProva.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAll")]
    public class MusicaController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMusicaService _musicaService;

        public MusicaController(IMapper mapper, IMusicaService musicaService)
        {
            _mapper = mapper;
            _musicaService = musicaService;
        }

        //GET: api/Musica
        [HttpGet]
        public ActionResult<IEnumerable<MusicaResponseViewModel>> Get()
        {
            var musicas = _musicaService.Listar();

            if (musicas == null) return NotFound();

            var mapMusica = _mapper.Map<List<MusicaResponseViewModel>>(musicas);

            return Ok(mapMusica);
        }

        //GET: api/Musica/5
        [HttpGet("{id}")]
        public ActionResult<MusicaResponseViewModel> Get(int id)
        {
            var musica = _musicaService.RecuperarPorId(id);

            if (musica == null) return NotFound();

            return Ok(_mapper.Map<MusicaResponseViewModel>(musica));
        }

        //PUT: api/Musica/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, MusicaRequestViewModel musicaViewModel)
        {
            if (id != musicaViewModel.Id) return BadRequest();

            try
            {
                var musica = _mapper.Map<Musica>(musicaViewModel);
           
                _musicaService.AlterarMusicaAutor(musica, musicaViewModel.IdsAutores);

                return Ok();
            }
            
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        //POST: api/Musica
        [HttpPost]
        public ActionResult Post(MusicaRequestViewModel musicaViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var musica = _mapper.Map<Musica>(musicaViewModel);

            _musicaService.IncluirMusicaAutor(musica, musicaViewModel.IdsAutores);

            return Created(string.Empty, null);
        }
       
    }
}
