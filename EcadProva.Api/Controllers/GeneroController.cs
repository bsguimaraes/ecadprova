﻿using AutoMapper;
using EcadProva.Api.ViewModel;
using EcadProva.Domain.Interfaces.Services;
using EcadProva.Domain.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace EcadProva.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GeneroController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IGeneroService _generoService;

        public GeneroController(IMapper mapper, IGeneroService generoService)
        {
            _mapper = mapper;
            _generoService = generoService;
        }

        // GET: api/Genero
        [HttpGet]
        public ActionResult<IEnumerable<GeneroViewModel>> Get()
        {
            var generos = _generoService.Listar();

            if (generos == null) return NotFound();

            var mapAutores = _mapper.Map<List<GeneroViewModel>>(generos);

            return Ok(mapAutores);
        }

        // GET: api/Genero/5
        [HttpGet("{id}")]
        public ActionResult<GeneroViewModel> Get(int id)
        {
            var genero = _generoService.RecuperarPorId(id);

            if (genero == null) return NotFound();

            return Ok(_mapper.Map<GeneroViewModel>(genero));
        }

        // POST: api/Genero
        [HttpPost]
        public ActionResult<Genero> Post(GeneroViewModel generoViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var genero = _mapper.Map<Genero>(generoViewModel);

            _generoService.Incluir(genero);

            return CreatedAtAction("Get", new { id = genero.Id }, genero);
        }
    }
}
