﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EcadProva.Domain.Models;
using EcadProva.Infra.Data.Context;

namespace EcadProva.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MusicaAutorController : ControllerBase
    {
        private readonly EcadProvaContext _context;

        public MusicaAutorController(EcadProvaContext context)
        {
            _context = context;
        }

        // GET: api/MusicaAutor
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<MusicaAutor>>> GetMusicaAutor()
        //{
        //return await _context.MusicaAutor.ToListAsync();
        //}

        // GET: api/MusicaAutor/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<MusicaAutor>> GetMusicaAutor(int id)
        //{
        //var musicaAutor = await _context.MusicaAutor.FindAsync(id);

        //if (musicaAutor == null)
        //{
        //    return NotFound();
        //}

        //return musicaAutor;
        //}

        // PUT: api/MusicaAutor/5
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutMusicaAutor(int id, MusicaAutor musicaAutor)
        //{
        //    if (id != musicaAutor.IdMusica)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(musicaAutor).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!MusicaAutorExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        // POST: api/MusicaAutor
        //[HttpPost]
        //public async Task<ActionResult<MusicaAutor>> PostMusicaAutor(MusicaAutor musicaAutor)
        //{
        //    //_context.MusicaAutor.Add(musicaAutor);
        //try
        //{
        //    await _context.SaveChangesAsync();
        //}
        //catch (DbUpdateException)
        //{
        //    if (MusicaAutorExists(musicaAutor.IdMusica))
        //    {
        //        return Conflict();
        //    }
        //    else
        //    {
        //        throw;
        //    }
        //}

        //return CreatedAtAction("GetMusicaAutor", new { id = musicaAutor.IdMusica }, musicaAutor);
        //  }

        // DELETE: api/MusicaAutor/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<MusicaAutor>> DeleteMusicaAutor(int id)
        //{
        //    var musicaAutor = await _context.MusicaAutor.FindAsync(id);
        //    if (musicaAutor == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.MusicaAutor.Remove(musicaAutor);
        //    await _context.SaveChangesAsync();

        //    return musicaAutor;
        //}

        //private bool MusicaAutorExists(int id)
        //{
        //    return _context.MusicaAutor.Any(e => e.IdMusica == id);
        //}
    }
}
