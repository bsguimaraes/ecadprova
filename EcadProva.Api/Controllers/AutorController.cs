﻿using AutoMapper;
using EcadProva.Api.ViewModel;
using EcadProva.Domain.Interfaces.Services;
using EcadProva.Domain.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace EcadProva.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AutorController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IAutorService _autorService;

        public AutorController(IMapper mapper, IAutorService autorService)
        {
            _mapper = mapper;
            _autorService = autorService;
        }

        // GET: api/Autor
        [HttpGet]
        public ActionResult<IEnumerable<AutorViewModel>> Get()
        {
            var autores = _autorService.Listar();

            if (autores == null) return NotFound();

            var mapAutores = _mapper.Map<List<AutorViewModel>>(autores);

            return Ok(mapAutores);
        }

        // GET: api/Autor/5
        [HttpGet("{id}")]
        public ActionResult<Autor> Get(int id)
        {
            var autor = _autorService.RecuperarPorId(id);

            if (autor == null) return NotFound();

            return Ok(_mapper.Map<AutorViewModel>(autor));
        }

        // POST: api/Autor
        [HttpPost]
        public ActionResult<AutorViewModel> Post(AutorViewModel AutorViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var autor = _mapper.Map<Autor>(AutorViewModel);

            _autorService.Incluir(autor);

            return Created(string.Empty, null);
        }
    }
}
