﻿using AutoMapper;
using EcadProva.Api.ViewModel;
using EcadProva.Domain.Models;

namespace EcadProva.Api.AutoMapper
{
    public class Mappings : Profile
    {
        public Mappings()
        {
            CreateMap<Musica, MusicaRequestViewModel>().ReverseMap();
            CreateMap<Musica, MusicaResponseViewModel>().ReverseMap();
            CreateMap<Autor, AutorViewModel>().ReverseMap();
            CreateMap<Categoria, CategoriaViewModel>().ReverseMap();
            CreateMap<MusicaAutor, MusicaAutorViewModel>().ReverseMap();
            CreateMap<Genero, GeneroViewModel>().ReverseMap();
        }
    }
}
