﻿using System.Collections.Generic;

namespace EcadProva.Domain.Models
{
    public class Categoria
    {
        public int Id { get; set; }
        public string Nome { get; set; }

        public List<Autor> Autores { get; set; }
    }
}
