﻿using System.Collections.Generic;

namespace EcadProva.Domain.Models
{
    public class Musica
    {
        public Musica()
        {
            MusicaAutores = new List<MusicaAutor>();
        }
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Nome { get; set; }
        public int GeneroId { get; set; }

        public Genero Genero { get; set; }
        public virtual List<MusicaAutor> MusicaAutores { get; set; }
    }
}
