﻿namespace EcadProva.Domain.Models
{
    public class MusicaAutor
    {
        public int IdMusica { get; set; }
        public int IdAutor { get; set; }

        public virtual Musica Musica { get; set; }
        public virtual Autor Autor { get; set; }
    }
}
