﻿using System.Collections.Generic;

namespace EcadProva.Domain.Models
{
    public class Genero
    {
        public int Id { get; set; }
        public string Nome { get; set; }

        public List<Musica> Musicas { get; set; }
    }
}
