﻿using EcadProva.Domain.Interfaces.Repositories.Base;
using EcadProva.Domain.Models;

namespace EcadProva.Domain.Interfaces.Repositories
{
    public interface IMusicaAutorRepository : IBaseRepository<MusicaAutor>
    {
        MusicaAutor RetornarPorMusicaAutor(int idAutor, int idMusica);
    }
}
