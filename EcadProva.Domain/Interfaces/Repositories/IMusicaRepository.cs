﻿using EcadProva.Domain.Interfaces.Repositories.Base;
using EcadProva.Domain.Models;

namespace EcadProva.Domain.Interfaces.Repositories
{
    public interface IMusicaRepository: IBaseRepository<Musica>
    {
    }
}
