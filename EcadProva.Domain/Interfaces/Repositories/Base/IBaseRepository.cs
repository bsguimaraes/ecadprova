﻿using System.Collections.Generic;

namespace EcadProva.Domain.Interfaces.Repositories.Base
{
    public interface IBaseRepository<T> where T : class
    {
        ICollection<T> Listar();
        T RecuperarPorId(int id);
        void Incluir(T obj);
        void Alterar(T obj);
        void Excluir(int id);
    }
}
