﻿using EcadProva.Domain.Interfaces.Services.Base;
using EcadProva.Domain.Models;

namespace EcadProva.Domain.Interfaces.Services
{
    public interface IAutorService: IBaseService<Autor>
    {
    }
}
