﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EcadProva.Domain.Interfaces.Services.Base
{
    public interface IBaseService<T> where T : class
    {
        ICollection<T> Listar();
        T RecuperarPorId(int id);
        void Incluir(T obj);
        void Alterar(T obj);
        void Excluir(int id);
    }
}
