﻿using EcadProva.Domain.Interfaces.Services.Base;
using EcadProva.Domain.Models;

namespace EcadProva.Domain.Interfaces.Services
{
    public interface IMusicaAutorService : IBaseService<MusicaAutor>
    {
        MusicaAutor RetornarPorMusicaAutor(int idAutor, int idMusica);
    }
}
