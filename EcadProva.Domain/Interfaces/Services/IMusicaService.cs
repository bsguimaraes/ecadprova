﻿using EcadProva.Domain.Interfaces.Services.Base;
using EcadProva.Domain.Models;
using System.Collections.Generic;

namespace EcadProva.Domain.Interfaces.Services
{
    public interface IMusicaService : IBaseService<Musica>
    {
        void IncluirMusicaAutor(Musica musica, List<int> listaIDsAutor);
        void AlterarMusicaAutor(Musica musica, List<int> listaIDsAutor);

    }
}
